#pragma once
#include "User.h"
#include "Service.h"
#include <vector>
#include <iostream>
#include <fstream>

using std::ofstream;
using std::ifstream;

//const char* format = "%d %B %Y";

class Client : public User {
public:
	Client() :User() {}
	Client(std::string nickname, std::string password) :User(nickname, password) {}

	void appNote(ClientNote n_note);

	int parseClient(std::ofstream& file);

	int unparseClient(std::ifstream &file, std::string login, std::string password);
	std::vector<ClientNote> getNotes();
private:
	std::vector<ClientNote> notes{};
};
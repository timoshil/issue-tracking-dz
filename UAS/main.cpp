#include "Service.h"
#include "Staff.h"
#include "User.h"
#include "Client.h"
#include <iostream>
#include <fstream>

void clientAuth();
void staffAuth();
void signUp();

int authScreen();
void clientScreen();
void staffScreen();

void addWorker();

Client current_user{};
Staff current_staff{};

bool authorized = false;
bool is_staff = false;

const char* clients_file = "new_file.txt";
const char* staffs_file = "new_staff_file.txt";

int main() {
	system("cls");
	int screen = -1;
	if (authorized == false){
		while (screen == -1) {
			screen = authScreen();
			system("cls");
		}
	}
	if (authorized) {
		if (!is_staff) { clientScreen(); }
		else {
			staffScreen();
		}
	}
}

void staffScreen() {
	std::cout << current_staff.getSpec() << "\nYOU: " << current_staff.getNote() << std::endl;
	std::cout << "\n1 - show my works\n";
	if (current_staff.getSpec() == "administrator") {
		std::cout << "2 - add a worker" << std::endl;
		std::cout << "3 - bind a work" << std::endl;
	}
	std::cout << "0 - logout\n>>> ";
	int choose{};
	std::cin >> choose;
	if (choose == 1) {
		system("cls");
		for (auto it : current_staff.getNotes()) {
			std::cout << it.time << " - ";
			if (it.service == Service::CLEANING) std::cout << "cleaning" << std::endl;
			if (it.service == Service::REPAIRING) std::cout << "repairing" << std::endl;
			if (it.service == Service::COLORING) std::cout << "coloring" << std::endl;
		}
	}
	if (current_staff.getSpec() == "administrator" && choose == 2) {
		addWorker();
	}

	if (current_staff.getSpec() == "administrator" && choose == 3) {
		std::ifstream ifile("notes.txt");
		std::string str;
		int id = 1;
		while (!ifile.eof()) {
			std::getline(ifile, str);
			std::cout << id << ") ";
			if (str[0] == '0') {
				std::cout << "cleaning";
			}
			if (str[0] == '1') {
				std::cout << "repairing";
			}
			if (str[0] == '2') {
				std::cout << "coloring";
			}
			str.erase(0, 2); 
			std::cout << " - " << str << std::endl;
			id++;
		}
		ifile.close();
		int n_id{};
		std::cout << "note id: ";
		std::cin >> n_id;
		system("cls");
		std::ifstream ifile2("notes.txt");
		str = "";
		id = 1;
		std::string service{};
		std::string date{};
		while (!ifile2.eof()) {
			std::getline(ifile2, str);
			if (id == n_id) {
				service = str[0];
				if (str[0] == '0') {
					std::cout << "cleaning";
				}
				if (str[0] == '1') {
					std::cout << "repairing";
				}
				if (str[0] == '2') {
					std::cout << "coloring";
				}
				str.erase(0, 2);
				date = str;
				std::cout << " - " << str << std::endl;
				break;
			}
			id++;
		}
		ifile2.close();
		ifstream sfile(staffs_file);
		std::string tmp{};
		id = 1;
		while (!sfile.eof()) {
			sfile >> tmp;
			if (tmp == "#!User#!") {
				std::cout << id << ") ";
				sfile >> tmp;
				sfile >> tmp;
				sfile >> tmp;//name
				std::cout << tmp << " ";
				sfile >> tmp;//surename
				sfile >> tmp;//phone
				std::cout << tmp << " [";
				sfile >> tmp;//spec
				std::cout << tmp;
				if (tmp == "0") std::cout << "washer";
				if (tmp == "1") std::cout << "engineer";
				if (tmp == "2") std::cout << "painter";
				if (tmp == "3") std::cout << "ADMIN";
				std::cout << "]" << std::endl;
				id++;
			}
		}
		sfile.close();
		int n_s_id{};
		std::cout << "Worker id: ";
		std::cin >> n_s_id;
		id = 1;
		ifstream in(staffs_file);
		ofstream out("temp.txt");
		while (!in.eof()) {
			in >> tmp;
			if (tmp == "#!User#!") {
				if (id == n_s_id) {
					out << tmp << "\n";//user
					in >> tmp;
					out << tmp << "\n";//login
					in >> tmp;
					out << tmp << "\n";//password
					in >> tmp;
					out << tmp << "\n";//name
					in >> tmp;
					out << tmp << "\n";//surename
					in >> tmp;
					out << tmp << "\n";//phone
					in >> tmp;
					if (tmp == "administrator") {
						current_staff.appNote(ClientNote{ Service(stoi(service)), date });
					}
					out << tmp << "\n";//spec
					in >> tmp;
					out << stoi(tmp) + 1 << "\n";//size
					int size = stoi(tmp);
					std::cout << "COUNT " << stoi(tmp) << " " << tmp << std::endl;
					for (int i{}; i < size; i++) {
						in >> tmp;
						out << tmp << "\n";//service
						in >> tmp;
						out << tmp << "\n";//date
					}
					out << service << "\n";
					out << date << "\n";
					break;
				}
				else {
					id++;
				}
			}
		}
		out.close();
		in.close();
		ifstream in2(staffs_file);
		ofstream out2("temp.txt", std::ios::app);
		id = 1; 
		while (!in2.eof()) {
			in2 >> tmp;
			if (tmp == "#!User#!") {
				if (id != n_s_id) {
					out2 << tmp << "\n";
					in2 >> tmp;
					out2 << tmp << "\n";//login
					in2 >> tmp;
					out2 << tmp << "\n";//password
					in2 >> tmp;
					out2 << tmp << "\n";//name
					in2 >> tmp;
					out2 << tmp << "\n";//surename
					in2 >> tmp;
					out2 << tmp << "\n";//phone
					in2 >> tmp;
					out2 << tmp << "\n";//spec
					in2 >> tmp;
					out2 << tmp << "\n";//size
					int size = stoi(tmp);
					for (int i{}; i < size; ++i) {
						in2 >> tmp;
						out2 << tmp << "\n";//service
						in2 >> tmp;
						out2 << tmp << "\n";//date
					}
				}
				id++;
			}
		}
		out2.close();
		in2.close();
		remove(staffs_file);
		rename("temp.txt", staffs_file);
		system("cls");
		std::cout << "Succesfull!" << std::endl;
	}

	if (choose == 0) {
		current_staff = Staff{};
		authorized = false;
		is_staff = false;
	}
	system("pause");
	main();
}

void addWorker() {
	std::string name{};
	std::string surename{};
	std::string phone;

	std::string login{};
	std::string password{};

	std::cout << "Create your nickname: ";
	std::cin >> login;
	std::cout << "Enter a password: ";
	std::cin >> password;

	Staff n_staff(login, password);

	std::cout << "Name: ";
	std::cin >> name;
	std::cout << "Surename: ";
	std::cin >> surename;
	std::cout << "Telephone: ";
	std::cin >> phone;
	int role{};
	std::cout << "Spec [0 - cleaning 1 - repairing 2 - coloring 3 - admin]\n>>> ";
	std::cin >> role;

	n_staff.setWHO(name, surename, phone);
	n_staff.setSpec(Roles(role));

	std::ofstream out(staffs_file, std::ios::app);

	n_staff.parseStaff(out);

	out.close();
	system("cls");
	std::cout << "Successfull!" << std::endl;
	system("pause");
}

void clientScreen() {
	std::cout << "YOU: " << current_user.getNote() << std::endl;
	std::cout << "\n1 - take a service\n\n2 - show my services\n3 - show workers list\n0 - logout\n>>> ";
	int choose{};
	std::cin >> choose;
	if (choose == 0) {
		current_user = Client();
		system("cls");
		std::cout << "You are logged out :(" << std::endl;
	}
	if (choose == 1) {
		system("cls");
		std::cout << "What need you in?\n1 - cleaning\n2 - repairing\n3 - coloring\n>>> ";
		int serv{};
		std::cin >> serv;
		system("cls");
		std::cout << "Write the date when you will visit us (dd.mm.yyyy)\n>>> ";
		std::string date;
		std::cin >> date;
		current_user.appNote(ClientNote{Service(serv-1), date});
		system("cls");
		std::string service;
		switch (serv) {
		case 1:
			service = "cleaning";
			break;
		case 2:
			service = "repairing";
			break;
		case 3:
			service = "coloring";
			break;
		default:
			service = "repairing";
			break;
		}
		std::ifstream in(clients_file);
		std::ofstream out("temp.txt", std::ios::app);
		std::string tmp;
		while (!in.eof()) {
			in >> tmp;
			if (tmp == "#!User#!") {
				in >> tmp;
				if (tmp != current_user.getLogin()) {
					out << "#!User#!" << "\n";
					out << tmp << "\n";//login
					in >> tmp;
					out << tmp << "\n";//password
					in >> tmp;
					out << tmp << "\n";//name
					in >> tmp;
					out << tmp << "\n";//surename
					in >> tmp;
					out << tmp << "\n";//phone
					in >> tmp;
					out << tmp << "\n";//size
					int size = stoi(tmp);
					for (int i{}; i < size; ++i) {
						in >> tmp;
						out << tmp << "\n";//service
						in >> tmp;
						out << tmp << "\n";//date
					}
				}
			}
		}
		current_user.parseClient(out);
		in.close();
		out.close();
		remove(clients_file);
		rename("temp.txt", clients_file);
		std::cout << "Good!\nYour " << service << " will be at " << date << "!\nDont't forget it!" << std::endl;
	}
	
	if (choose == 2) {
		system("cls");
		for (auto it : current_user.getNotes()) {
			std::cout << it.time << " - ";
			if (it.service == Service::CLEANING) std::cout << "cleaning" << std::endl;
			if (it.service == Service::REPAIRING) std::cout << "repairing" << std::endl;
			if (it.service == Service::COLORING) std::cout << "coloring" << std::endl;
		}
	}

	if (choose == 3) {
		system("cls");
		std::cout << "Workers!" << std::endl;
	}

	if (choose == 0) {
		current_user = Client{};
		authorized = false;
	}
	system("pause");
	main();
}

int authScreen() {
	bool auth = true;
	while (auth) {
		int choose{};
		std::cout << "Log In as:\n1 - client\n2 - personal\n\n3-Sign Up\n\n>>> ";
		std::cin >> choose;
		switch (choose)
		{
		case 1:
			system("cls");
			clientAuth();
			return 1;
		case 2:
			system("cls");
			staffAuth();
			return 2;
		case 3:
			system("cls");
			signUp();
			return 3;
		default:
			std::cout << "Wrong command!" << std::endl;
			return -1;
		}
	}
	return -1;
}

void signUp() {
	std::string name{};
	std::string surename{};
	std::string phone;

	std::string login{};
	std::string password{};

	std::cout << "Create your nickname: ";
	std::cin >> login;
	std::cout << "Enter a password: ";
	std::cin >> password;

	Client n_client(login, password);

	system("cls");
	std::cout << "Nice! Now tell about you!" << std::endl;

	std::cout << "Name: ";
	std::cin >> name;
	std::cout << "Surename: ";
	std::cin >> surename;
	std::cout << "Telephone: ";
	std::cin >> phone;

	n_client.setWHO(name, surename, phone);

	std::ofstream out(clients_file, std::ios::app);
	
	n_client.parseClient(out);

	out.close();
	system("cls");
	std::cout << "Successfull!\nWelcome!" << std::endl;
	current_user = n_client;
	authorized = true;
	system("pause");
}

// ������� ��� �����

void clientAuth() {
	Client o_client{};
	bool reading = true;
	std::string login{}, password{};
	std::cout << "Login: ";
	std::cin >> login;
	std::cout << "Password: ";
	std::cin >> password;
	std::ifstream in(clients_file);
	int res{};
	try {
		res = o_client.unparseClient(in, login, password);
	}
	catch (const char* err) {
		std::cout << err << std::endl;
		system("pause");
	}
	
	in.close();

	if (res == 1) {
		std::cout << "You are logged in as " << o_client.getLogin() << std::endl;
		current_user = o_client;
		authorized = true;
	}
	if (res == -1) {
		std::cout << "Something goes wrong!" << std::endl;
	}
	main();
}

void staffAuth() {
	Staff o_staff{};
	bool reading = true;
	std::string login{}, password{};
	std::cout << "Login: ";
	std::cin >> login;
	std::cout << "Password: ";
	std::cin >> password;
	std::ifstream in(staffs_file);
	int res{};
	try {
		res = o_staff.unparseStaff(in, login, password);
	}
	catch (const char* err) {
		std::cout << err << std::endl;
		system("pause");
	}

	in.close();

	if (res == 1) {
		std::cout << "You are logged in as " << o_staff.getLogin() << std::endl;
		current_staff = o_staff;
		authorized = true;
		is_staff = true;
	}
	if (res == -1) {
		std::cout << "Something goes wrong!" << std::endl;
	}
	system("pause");
}
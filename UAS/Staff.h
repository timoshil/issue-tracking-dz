//���� ��������� ������ Staff
#pragma once
#include "User.h"
#include "Service.h"
#include <fstream>
#include <vector>

class Staff : public User {
public:
	Staff():User() {
		spec = Roles::NONE;
	}
	Staff(std::string nickname, std::string password) :User(nickname, password) {
		spec = Roles::NONE;
	}
	std::string getSpec();
	int unparseStaff(std::ifstream& file, std::string login, std::string password);
	void parseStaff(std::ofstream& file);

	void setSpec(Roles n_spec);
	std::vector<ClientNote> getNotes();
	void appNote(ClientNote n_note);
private:
	Roles spec{};
	std::vector<ClientNote> notes{};
};
//���� ��������� ������ Service
#pragma once
#include <string>

enum class Roles {
	WASHER,
	ELECTRICIAN,
	PAINTER,
	ADMIN,
	NONE
};

enum class Service {
	CLEANING,
	REPAIRING,
	COLORING,
	NONE
};

struct ClientNote {
	Service service;
	std::string time;
};
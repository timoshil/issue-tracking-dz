#include "Client.h"

void Client::appNote(ClientNote n_note) {
	notes.push_back(n_note);
	std::ofstream ofile("notes.txt", std::ios::app);
	ofile << int(n_note.service) << " " << n_note.time << "\n";
}

int Client::parseClient(std::ofstream &file) {
	file << "#!User#!\n";
	file << m_nickname << "\n";
	file << m_password << "\n";
	file << m_name << "\n";
	file << m_surename << "\n";
	file << m_phone << "\n";

	file << notes.size() << "\n";

	for (const auto& it : notes) {
		file << int(it.service) << "\n";
		file << it.time << "\n";
	}

	return 0;
}

int Client::unparseClient(std::ifstream &file, std::string login, std::string password) {
	while (!file.eof()) {
		std::string tmp{};
		file >> tmp;
		if (tmp == "#!User#!") {
			file >> tmp;
			if (tmp == login) {
				m_nickname = tmp;
				file >> tmp;
				if (tmp == password) {
					m_password = tmp;
					file >> m_name;
					file >> m_surename;
					file >> m_phone;
					int size_vector{};
					file >> size_vector;
					ClientNote temp{};
					for (int i{}; i < size_vector; ++i) {
						int ser_int{};
						file >> ser_int;
						file >> temp.time;

						temp.service = Service(ser_int);
						notes.push_back(temp);
					}
					return 1;
				}
				else {
					throw "Wrong password!";
					return -1;
				}
			}
			else {
				continue;
			}
		}
		else {
			continue;
		}
		throw "This user doesn't exis :(";
		return -1;
	}
	
	/*file >> m_name;
	file >> m_nickname;
	file >> m_password;
	file >> m_phone;
	file >> m_surename;

	int size_vector{};

	file >> size_vector;

	ClientNote tmp{};

	for (int i{}; i < size_vector; ++i) {
		
		int ser_int{};

		file >> ser_int;
		file >> tmp.time;

		tmp.service = Service(ser_int);

		notes.push_back(tmp);
	}
	*/

	return 0;
}

std::vector<ClientNote> Client::getNotes() {
	return notes;
}
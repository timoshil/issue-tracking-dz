//���������� ������ Staff

#include "Staff.h"

std::string Staff::getSpec() {
	if (spec == Roles::ADMIN) { return "administrator"; }
	if (spec == Roles::WASHER) { return "washer"; }
	if (spec == Roles::ELECTRICIAN) { return "electrician"; }
	if (spec == Roles::PAINTER) { return "painter"; }
	if (spec == Roles::NONE) { return "no role"; }
}

void Staff::setSpec(Roles n_spec) {
	spec = n_spec;
}

int Staff::unparseStaff(std::ifstream& file, std::string login, std::string password) {
	while (!file.eof()) {
		std::string tmp{};
		file >> tmp;
		if (tmp == "#!User#!") {
			file >> tmp;
			if (tmp == login) {
				m_nickname = tmp;
				file >> tmp;
				if (tmp == password) {
					m_password = tmp;
					file >> m_name;
					file >> m_surename;
					file >> m_phone;
					int i_spec{};
					file >> i_spec;
					spec = Roles(i_spec);
					int size_vector{};
					file >> size_vector;
					ClientNote temp{};
					for (int i{}; i < size_vector; ++i) {
						int ser_int{};
						file >> ser_int;
						file >> temp.time;

						temp.service = Service(ser_int);
						notes.push_back(temp);
					}
					return 1;
				}
				else {
					throw "Wrong password!";
					return -1;
				}
			}
			else {
				continue;
			}
		}
		else {
			continue;
		}
		throw "This user doesn't exis :(";
		return -1;
	}
	return 0;
}


void Staff::parseStaff(std::ofstream& file) {
	file << "#!User#!\n";
	file << m_nickname << "\n";
	file << m_password << "\n";
	file << m_name << "\n";
	file << m_surename << "\n";
	file << m_phone << "\n";
	file << int(spec) << "\n";

	file << notes.size() << "\n";

	for (const auto& it : notes) {
		file << int(it.service) << "\n";
		file << it.time << "\n";
	}

}

void Staff::appNote(ClientNote n_note) {
	notes.push_back(n_note);
}

std::vector<ClientNote> Staff::getNotes() {
	return notes;
}
//���� ��������� ������ User
#include <string>
#pragma once

struct UserID {
	std::string login;
	std::string password;
};

class User {
public:
	User();
	User(std::string nickname, std::string password);

	std::string getNote();
	UserID getUserID();

	std::string getPassword();
	std::string getLogin();

	void changePassword(std::string n_password);
	void setWHO(std::string name, std::string surename, std::string phone);
protected:
	std::string m_nickname;
	std::string m_password;
	std::string m_name;
	std::string m_surename;
	std::string m_phone;
};